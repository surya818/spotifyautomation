package org.sikuli.api.spotify_auto;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.Relative;
import org.sikuli.api.ScreenLocation;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.Target;
import org.sikuli.api.TextTarget;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;
import org.sikuli.api.visual.Canvas;
import org.sikuli.api.visual.DesktopCanvas;

public class SpotifyMainScreen {

	static ScreenRegion s;
	static Keyboard kb;
	static Mouse m;

	public List<ScreenRegion> searchFunc(ScreenRegion s, String criteria) {
		if (criteria.equals("")) {
			s = new DesktopScreenRegion();
			Target srchBox = new ImageTarget(getClass().getResource(
					"/SearchButton.png"));
			ScreenRegion box = s.wait(srchBox, 2000);
			Canvas c = new DesktopCanvas();
			// c.addLabel(box, "Text field").display(4);
			Mouse m = new DesktopMouse();
			m.click(box.getCenter());
			Keyboard kb = new DesktopKeyboard();
			kb.type(criteria);
			// m.click(box.getCenter());
			Target emptyString = new ImageTarget(getClass().getResource(
					"/EmptyStringSearch.png"));
			ScreenRegion emptyStringSrch = s.wait(emptyString, 3000);
			// c.addLabel(emptyStringSrch, "Empty string").display(4);
			List<ScreenRegion> li = new ArrayList<ScreenRegion>();
			li.add(emptyStringSrch);
			return li;

		} else {
			s = new DesktopScreenRegion();
			Target srchBox = new ImageTarget(getClass().getResource(
					"/SearchButton.png"));
			ScreenRegion box = s.wait(srchBox, 4000);
			Canvas c = new DesktopCanvas();
			// c.addLabel(box, "Text field").display(4);

			/*
			 * Target scroll=new
			 * ImageTarget(getClass().getResource("/MainScreenScroll.png"));
			 * 
			 * while(true){ s=new DesktopScreenRegion(); ScreenRegion
			 * scrollBar=s.wait(scroll, 2000); if(scrollBar!=null){
			 * c.addBox(scrollBar).display(1); break; } }
			 */
			Mouse m = new DesktopMouse();
			m.click(box.getCenter());
			Keyboard kb = new DesktopKeyboard();
			kb.type(criteria);
			Target showResults = new ImageTarget(getClass().getResource(
					"/ShowResults.png"));
			ScreenRegion allResults = s.wait(showResults, 8000);
			m.click(allResults.getCenter());
			Target ResultsPage = new ImageTarget(getClass().getResource(
					"/ResultsPage.png"));
			ScreenRegion resultsPage = new DesktopScreenRegion();
			// s.wait(ResultsPage, 4000);
			// BufferedImage resultsImage=resultsPage.capture();

			// ScreenRegion srchResults=new DefaultScreenRegion(new
			// StaticImageScreen(resultsImage));
			Target areResultsFound = new ImageTarget(getClass().getResource(
					"/NoMatchFound.png"));
			ScreenRegion validateFail = resultsPage.wait(areResultsFound, 3000);
			Target critText = new TextTarget(criteria);

			// System.out.println(validateFail.equals(null));
			List<ScreenRegion> li = new ArrayList<ScreenRegion>();
			if (validateFail == null) {
				// c.addLabel(resultsRegion,
				// "Results for "+criteria).display(1);
				List<ScreenRegion> resultsList = resultsPage.findAll(critText);
				// for(ScreenRegion s1: resultsList){
				// c.addLabel(s1, "Match found").display(1);
				// }
				return resultsList;
			}

			else {
				return li;
			}
		}
	}

	//

	public Set<Double> songsPlays() {

		s = new DesktopScreenRegion();
		Canvas c = new DesktopCanvas();
		Target songSelection = new ImageTarget(getClass().getResource(
				"/PlaySong.png"));

		ScreenRegion songSection = s.wait(songSelection, 2000);
		ScreenRegion trackSel = Relative.to(songSection).below(550)
				.getScreenRegion();
		c.addLabel(trackSel, "Song section0").display(3);

		m = new DesktopMouse();
		m.doubleClick(trackSel.getCenter());
		s = new DesktopScreenRegion();
		Target bar = new ImageTarget(getClass().getResource("/SongSel.png"));
		ScreenRegion songBar = s.wait(bar, 6000);
		c.addBox(songBar).display(1);
		// Target slide=new ImageTarget(getClass().getResource("/Slides.png"));
		Set<Double> slideMotion = new HashSet<Double>();
		for (int i = 0; i < 5; i++) {
			s = new DesktopScreenRegion();
			ScreenRegion songBars = s.wait(bar, 2000);
			c.addBox(songBars).display(1);
			slideMotion.add(songBars.getBounds().getX());
			// System.out.println(songBars.getBounds());
		}
		// System.out.println(slideMotion);
		// System.out.println(slideMotion.size()+": Size of the list");

		return slideMotion;

	}

	public void exit() {
		s = new DesktopScreenRegion();
		Target close = new ImageTarget(getClass().getResource("/CloseMain.png"));

		ScreenRegion closeMain = s.wait(close, 2000);
		// System.out.println(closeMain.getBounds());
		ScreenRegion exitMenu = Relative.to(closeMain).below(35)
				.getScreenRegion();
		Canvas c = new DesktopCanvas();
		c.addBox(exitMenu).display(1);
		m = new DesktopMouse();
		m.click(exitMenu.getCenter());
		s = new DesktopScreenRegion();
		Target logO = new ImageTarget(getClass().getResource("/Logout.png"));
		ScreenRegion logOut = s.wait(logO, 2000);
		m.click(logOut.getCenter());

		Target spotWin = new ImageTarget(getClass().getResource("/SpotWin.png"));
		s = new DesktopScreenRegion();
		ScreenRegion spotWins = s.wait(spotWin, 4000);
		c.addBox(spotWins).display(2);
		Target closeWin = new ImageTarget(getClass().getResource(
				"/CloseMain.png"));

		ScreenRegion closeSpotWin = spotWins.wait(closeWin, 3000);

		m.click(closeSpotWin.getCenter());
	}

	/*
	 * 
	 * public boolean songPlays(){
	 * 
	 * s=new DesktopScreenRegion(); Canvas c=new DesktopCanvas(); Target
	 * songSelection=new ImageTarget(getClass().getResource("/PlaySong.png"));
	 * ScreenRegion songSection=s.wait(songSelection, 2000); ScreenRegion
	 * trackSel=Relative.to(songSection).below(550).getScreenRegion();
	 * c.addLabel(trackSel, "Song section0").display(3); Target before=new
	 * ImageTarget(getClass().getResource("/BeforeSong.png")); ScreenRegion
	 * beforeSong=s.wait(before, 1000);
	 * 
	 * m=new DesktopMouse(); m.doubleClick(trackSel.getCenter()); s=new
	 * DesktopScreenRegion(); Target after=new
	 * ImageTarget(getClass().getResource("/AfterSong.png")); ScreenRegion
	 * afterSong=s.wait(after, 1000); if(afterSong!=null && beforeSong!=null){
	 * return true; } return false;
	 * 
	 * }
	 */

}
