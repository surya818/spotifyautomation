package org.sikuli.api.spotify_auto;

//package classes;
import static org.sikuli.api.API.browse;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.sikuli.api.DefaultScreenRegion;
import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.Relative;
//import org.sikuli.api.StaticImageScreen;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.Target;
import org.sikuli.api.TextTarget;
import org.sikuli.api.robot.Key;
import org.sikuli.api.robot.KeyModifier;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;
import org.sikuli.api.visual.Canvas;
import org.sikuli.api.visual.DesktopCanvas;

public class SpotifyLoginScreen {
	ScreenRegion s;
	Keyboard kb;
	Mouse m;
	String invalidLoginLoc;
	String SPOTIFY_LOC = "C:\\Users\\rekhasu\\AppData\\Roaming\\Spotify\\spotify.exe";
	String UNAME_LOC;
	String PWD_LOC;
	String MENU_LIST_LOC;

	public static void main(String[] args) throws MalformedURLException,
			InterruptedException {
		SpotifyLoginScreen spot = new SpotifyLoginScreen();
		spot.openApp();
		ScreenRegion sr = spot.loginWithValidAccounts("surya818", "Rekha!23");
		// System.out.println(sr==null);

	}

	// This method launches the Spotify Screen
	public ScreenRegion openApp() throws InterruptedException,
			MalformedURLException {

		Thread.sleep(10000);
		browse(new URL("file:\\" + SPOTIFY_LOC));

		s = new DesktopScreenRegion();
		return s;
	}

	public ScreenRegion loginWithValidAccounts(String uname, String pwd)
			throws InterruptedException, MalformedURLException {

		// Open Spotify window
		s = openApp();

		// Enter the Password
		Thread.sleep(3000);
		Canvas c = new DesktopCanvas();

		s = new DesktopScreenRegion();
		Target pwdImg = new ImageTarget(getClass().getResource("/Password.png"));
		ScreenRegion pwdReg = s.wait(pwdImg, 4000);
		// c.addBox(pwdReg).display(2);
		kb = new DesktopKeyboard();
		m = new DesktopMouse();
		m.click(pwdReg.getCenter());
		kb.type(pwd);
		Thread.sleep(3000);
		ScreenRegion unames = Relative.to(pwdReg).above(70).getScreenRegion();
		// c.addBox(unames).display(2);
		m.click(unames.getCenter());
		kb.type(Key.END);
		for (int i = 0; i < 30; i++) {
			kb.type(Key.BACKSPACE);
		}
		kb.type(Key.TAB);
		// Enter the Username
		Target unameImg = new ImageTarget(getClass().getResource(
				"/UserName1.png"));
		m.click(s.wait(unameImg, 4000).getCenter());

		kb.type(uname);
		// Click on the Login Button
		Target loginBtn = new ImageTarget(getClass().getResource(
				"/LoginBtn.png"));
		s = new DesktopScreenRegion();
		s = s.wait(loginBtn, 4000);
		m = new DesktopMouse();
		m.click(s.getCenter());

		// Identifying the FileMenu Region, as this is one of the static regions
		// of the Spotify Home screen
		Target errMsg = new ImageTarget(getClass().getResource(
				"/LoginFailed.png"));
		s = new DesktopScreenRegion();
		// BufferedImage loginImg=s.capture();
		// ScreenRegion isloginSuccess=new DefaultScreenRegion(new
		// StaticImageScreen(loginImg));

		ScreenRegion errorMsg = s.wait(errMsg, 40000);
		System.out.println(errorMsg == null);
		if (errorMsg != null) {
			// Canvas c=new DesktopCanvas();
			c.addLabel(errorMsg, "Error login").display(3);
			;
			return errorMsg;
		}
		// System.out.println("Something thing");

		Target homeTarget = new ImageTarget(getClass().getResource(
				"/FileMenu.png"));
		s = s.wait(homeTarget, 5000);
		// Rekha!23 Canvas c=new DesktopCanvas();
		c.addLabel(s, "File Menu, rocks!!!").display(5);
		;

		return s;

	}

	public void closeWindow() {
		s = new DesktopScreenRegion();
		Target closeWin = new ImageTarget(getClass().getResource(
				"/CloseWin.png"));
		ScreenRegion closeIt = s.wait(closeWin, 2000);
		m = new DesktopMouse();
		Canvas c = new DesktopCanvas();
		// c.addLabel(closeIt, "Close it, mate!!!").display(3);
		m.click(closeIt.getCenter());
		// System.out.println("Window closed");
	}

}
