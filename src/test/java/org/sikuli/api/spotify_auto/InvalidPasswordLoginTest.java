package org.sikuli.api.spotify_auto;


import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

import org.sikuli.api.ScreenRegion;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class InvalidPasswordLoginTest {
	 SpotifyLoginScreen spot;
	 SpotifyMainScreen newMain;
	 ScreenRegion sr;
	 TestDataCreator tdc;
 
  
  @Test
  public void invalidPwdLogin() throws InterruptedException, IOException {
		 Map<String,String> data=tdc.createTestData("TestData.txt");
		  String valid_uname=data.get("Valid_Username");
		  String invalid_pwd=data.get("Invalid_Password");
		  spot.openApp();
		  ScreenRegion bob=spot.loginWithValidAccounts(valid_uname,invalid_pwd);
		  Assert.assertNotNull(bob);
		  
	  }

  @BeforeMethod
  public void beforeClass() {
	  spot=new SpotifyLoginScreen();
	  tdc=new TestDataCreator();
	    
  }

  @AfterMethod
  public void afterClass() {
	  spot.closeWindow();

  }

}
