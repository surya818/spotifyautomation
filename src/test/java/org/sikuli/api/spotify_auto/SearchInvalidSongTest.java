package org.sikuli.api.spotify_auto;


import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ScreenRegion;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class SearchInvalidSongTest {
	 SpotifyLoginScreen spot;
	 SpotifyMainScreen newMain;
	 ScreenRegion sr;
	 TestDataCreator tdc;
  @Test
  public void validCredentialsLogin() throws InterruptedException, IOException {
	  Map<String,String> data=tdc.createTestData("TestData.txt");
	  String uname=data.get("Valid_Username");
	  String pwd=data.get("Valid_Password");
	  spot.openApp();
	  ScreenRegion sr=spot.loginWithValidAccounts(uname,pwd);
	  //newMain.exit();
	  Assert.assertNotNull(sr);
	 
	  
  }
  
  
  @Test(dependsOnMethods = {"validCredentialsLogin"})
  public void searchInvalidSong() throws InterruptedException, IOException {
	  //sr=new DesktopScreenRegion();
	  Map<String,String> data=tdc.createTestData("TestData.txt");
	  String invalidSong=data.get("Invalid_Song");
	  List<ScreenRegion>totalMatches=newMain.searchFunc(sr, invalidSong);
	  Assert.assertTrue((totalMatches.size()==0), "Empty List means no matches found");
	  
  }
  
  @BeforeMethod
  public void beforeClass() {
	  spot=new SpotifyLoginScreen();
	  newMain=new SpotifyMainScreen();
	  tdc=new TestDataCreator();
	    
  }
  @AfterClass
  public void tearDown(){
	  
	  newMain.exit();
	  
  }

  

}
