package org.sikuli.api.spotify_auto;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ScreenRegion;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class SpotifyValidEndToEndByArtistNameTest {
	SpotifyLoginScreen spot;
	SpotifyMainScreen newMain;
	ScreenRegion sr;
	TestDataCreator tdc;
	Map<String, String> data;

	@Test
	public void validCredentialsLogin() throws InterruptedException,
			IOException {
		String uname = data.get("Valid_Username");
		String pwd = data.get("Valid_Password");
		spot.openApp();
		ScreenRegion sr = spot.loginWithValidAccounts(uname, pwd);
		Assert.assertNotNull(sr);

	}

	@Test(dependsOnMethods = { "validCredentialsLogin" })
	public void searchSong() throws MalformedURLException, InterruptedException {
		String artist = data.get("Valid_Song_byBand");
		List<ScreenRegion> totalMatches = newMain.searchFunc(sr, artist);
		Assert.assertTrue((totalMatches.size() > 0),
				"Empty List means no matches found");

	}

	@Test(dependsOnMethods = { "searchSong" })
	public void playSong() {
		Set<Double> slideLocs = newMain.songsPlays();
		Assert.assertTrue((slideLocs.size() > 1), "Slide bar didn't move");
	}

	@BeforeMethod
	public void beforeClass() throws IOException {
		spot = new SpotifyLoginScreen();
		newMain = new SpotifyMainScreen();
		tdc = new TestDataCreator();
		data = tdc.createTestData("TestData.txt");
	}

	@AfterClass
	public void tearDown() {

		newMain.exit();

	}

}
