package org.sikuli.api.spotify_auto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class TestDataCreator {
public static void main(String[]args) throws IOException{
	TestDataCreator td=new TestDataCreator();
	td.createTestData("TestData.txt");
}
	public Map<String,String> createTestData(String filename) throws IOException{
		File f=new File(filename);
		Map<String,String> testdata=new HashMap<String,String>();
		BufferedReader br=new BufferedReader(new FileReader(f));
		String tmp="";
		while((tmp=br.readLine())!=null){
			//System.out.println(tmp);
			String[]split=tmp.split(":");
			testdata.put(split[0], split[1]);
		}
		return testdata;
	}

}
