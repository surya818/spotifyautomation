package org.sikuli.api.spotify_auto;


import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

import org.sikuli.api.ScreenRegion;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class InvalidUserNameLoginTest {
	 SpotifyLoginScreen spot;
	 SpotifyMainScreen newMain;
	 ScreenRegion sr;
	 TestDataCreator tdc;
 @Test
  public void invalidUserNameLogin() throws InterruptedException, IOException {
	 Map<String,String> data=tdc.createTestData("TestData.txt");
	  String invalid_uname=data.get("Invalid_Username");
	  String valid_pwd=data.get("Valid_Password");
	  spot.openApp();
	  ScreenRegion bob=spot.loginWithValidAccounts(invalid_uname,valid_pwd);
	  Assert.assertNotNull(bob);
	  
  }
  
  @BeforeMethod
  public void beforeClass() {
	  spot=new SpotifyLoginScreen();
	  tdc=new TestDataCreator();
	    
  }

  @AfterMethod
  public void afterClass() {
	  spot.closeWindow();

  }

}
